'use strict';

/**
 * @ngdoc overview
 * @name angularNewsApp
 * @description
 * # angularNewsApp
 *
 * Main module of the application.
 */
angular
  .module('angularNewsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'template/main.html',
        controller: 'MainCtrl'
//        controllerAs: 'main'
      })
      .when('/mail', {
        templateUrl: 'template/list/mail.html',
        controller: 'mailCtrl'
//        controllerAs: 'about'
      })
      .when('/socialnet', {
        templateUrl: 'template/list/socialnet.html',
        controller: 'socialnetCtrl'
//        controllerAs: 'about'
      })
      .when('/videotv', {
        templateUrl: 'template/list/videotv.html',
        controller: 'videotvCtrl'
//        controllerAs: 'about'
      })
      .when('/travel', {
        templateUrl: 'template/list/travel.html',
        controller: 'travelCtrl'
//        controllerAs: 'about'
      })
      .when('/anal', {
        templateUrl: 'template/list/anal.html',
        controller: 'analCtrl'
//        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
