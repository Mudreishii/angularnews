angular.module('angularNewsApp')
  .directive('mainmenu', function(){
    return {
      restrict: 'E',
      controller: 'menu',
      templateUrl: '../template/menu.html'
    }
  })
  .directive('maincontent', function(){
    return {
      restrict: 'E',
//      controller: 'content',
      templateUrl: '../template/content.html'
    }
  });